﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using supermarketPricingKata.converter;
using supermarketPricingKata.product.facories;

namespace supermarketPricingKataTests
{
    [TestClass]
    public class ConverterTest : AbstractTest
    {
        private ProductFacory _productFacory;
        private UnitConverter _unitConverter;

        [TestInitialize]
        public void Setup()
        {
            _productFacory = new ProductFacory();
            _unitConverter = new UnitConverter();
        }

        [TestMethod]
        public void ThousandGramsShouldBeConvertedTo1Kilo()
        {
            decimal convertedUnitValue = _unitConverter.convert(_productFacory.Apple(), WeightUnits.Gram, 1000);
            AssertDiff(convertedUnitValue, new decimal(1));
        }

        [TestMethod]
        public void OneKiloShouldBeConvertedToThousandGrams()
        {
            decimal convertedUnitValue = _unitConverter.convert(_productFacory.Rice(), WeightUnits.Kilo, 1);
            AssertDiff(convertedUnitValue, new decimal(1000));
        }

        [TestMethod]
        public void FiftyOunceShouldBeConvertedToCorrectAmountInKilo()
        {
            decimal convertedValue = _unitConverter.convert(_productFacory.Apple(), WeightUnits.Ounce, 50);
            AssertDiff(convertedValue, new decimal(5));
        }
    }
}