﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using supermarketPricingKata.cart;
using supermarketPricingKata.converter;
using supermarketPricingKata.discount.cart;
using supermarketPricingKata.price;
using supermarketPricingKata.product;
using supermarketPricingKata.product.facories;

namespace supermarketPricingKataTests
{
    [TestClass]
    public class ProductTests : AbstractTest
    {
        private ProductFacory _productFacory;

        [TestInitialize]
        public void Setup()
        {
            _productFacory = new ProductFacory();
        }

        [TestMethod]
        public void ShouldInitializeDiscountByNullObjectPattern()
        {
            var products = new List<IProduct>
            {
                _productFacory.Milk(),
                _productFacory.Pasta(),
                _productFacory.Apple(),
                _productFacory.Cola(),
                _productFacory.MandMs(),
                _productFacory.PastaSauce(),
                _productFacory.Rice()
            };

            foreach (IProduct product in products)
            {
                Assert.IsNotNull(product.GetDiscount());
            }
        }

        [TestMethod]
        public void ShouldReturnCorrectPriceForOneMilk()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 1);
            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, _productFacory.Milk().GetUnitPrice());
        }

        [TestMethod]
        public void ShouldReturnCorrectPriceForTwoMilk()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, _productFacory.Milk().GetUnitPrice()*2);
        }

        [TestMethod]
        public void AddingThreeTimesTwoMilkShouldReturnCorrectTotalPrice()
        {
            decimal milkUnitPrice = _productFacory.Milk().GetUnitPrice();
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());

            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);

            decimal calculateTotalPrice3 = cart.CalculateTotalPrice();
            AssertDiff(calculateTotalPrice3, milkUnitPrice*6);
        }

        [TestMethod]
        public void AddingTwoCartDiscountsShouldResultInOnlyOneDiscount()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.SetCartDiscount(new SpecificCartDiscount(5));
            cart.SetCartDiscount(new SpecificCartDiscount(5));

            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Bottle, 1);

            decimal totalPrice = cart.CalculateTotalPrice();

            AssertDiff(totalPrice, _productFacory.Milk().GetUnitPrice()* 95 /100);
        }
    }
}