﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace supermarketPricingKataTests
{
    public class AbstractTest
    {
        protected void AssertDiff(decimal a, decimal b, decimal allowedDifference = (decimal) 0.01)
        {
            decimal actualDifference = Math.Abs(a - b);
            Assert.IsTrue(actualDifference < allowedDifference,
                "first: " + a + " second: " + b + " difference: " + actualDifference + " allowed difference: " +
                allowedDifference);
        }
    }
}