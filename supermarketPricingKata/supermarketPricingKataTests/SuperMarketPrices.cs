﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using supermarketPricingKata.cart;
using supermarketPricingKata.converter;
using supermarketPricingKata.discount.cart;
using supermarketPricingKata.discount.product;
using supermarketPricingKata.price;
using supermarketPricingKata.product.facories;

namespace supermarketPricingKataTests
{
    /* Shopping list
    * - Apples      980gram   2,49 per kilo
    * - Milk        2 packs   1,29 per pack
    * - Pasta       1 pack    0,99 per pack
    * - Pasta sauce 1 jar     1,39 per jar
    * - Cola        4 bottles 1,50 per bottle
    * - M&m's       7 sacks   2,10 per sack
    */

    [TestClass]
    public class SuperMarketPrices : AbstractTest
    {
        private ProductFacory _productFacory;

        [TestInitialize]
        public void Setup()
        {
            _productFacory = new ProductFacory();
        }

        // Add products to shopping cart 
        // Calculate total price
        [TestMethod]
        public void CalculateSimpleTotal()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Apple(), WeightUnits.Gram, 980);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Pasta(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.PastaSauce(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.Cola(), QuantityUnit.Packs, 4);
            cart.AddProduct(_productFacory.MandMs(), QuantityUnit.Packs, 7);

            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, new decimal(28.10));
        }

        // Add products to shopping cart 
        // Give 15% discount on the pasta
        // Calculate total price

        [TestMethod]
        public void CalculateTotalWithProductDiscount()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Apple(), WeightUnits.Gram, 980);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Pasta(new SpecificProductDiscount(new decimal(15))), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.PastaSauce(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.Cola(), QuantityUnit.Packs, 4);
            cart.AddProduct(_productFacory.MandMs(), QuantityUnit.Packs, 7);

            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, new decimal(27.95));
        }

        // Add products to shopping cart 
        // Give 10% dicount on the total price
        // Calculate total price
        [TestMethod]
        public void CalculateTotalWithDiscount()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Apple(), WeightUnits.Gram, 980);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Pasta(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.PastaSauce(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.Cola(), QuantityUnit.Packs, 4);
            cart.AddProduct(_productFacory.MandMs(), QuantityUnit.Packs, 7);

            cart.SetCartDiscount(new SpecificCartDiscount(new decimal(10)));

            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, new decimal(25.29));
        }

        // Add products to shopping cart 
        // Add M&Ms discount: 3 packs for 5 euro
        // Calculate total price
        [TestMethod]
        public void CalculateTotalWithQuantityDiscount()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Apple(), WeightUnits.Gram, 980);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Pasta(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.PastaSauce(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.Cola(), QuantityUnit.Packs, 4);
            cart.AddProduct(_productFacory.MandMs(new AmountForSpecificPriceProductDiscount(3, 5)), QuantityUnit.Packs,
                7);

            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, new decimal(25.50));
        }

        // Add products to shopping cart 
        // Add Cola discount 3 bottles for the price of 2
        // Calculate total price
        [TestMethod]
        public void CalculateTotalWithFreeProduct()
        {
            var cart = new ShoppingCart(new ProductPriceCalculator(), new CartPriceCalculator());
            cart.AddProduct(_productFacory.Apple(), WeightUnits.Gram, 980);
            cart.AddProduct(_productFacory.Milk(), QuantityUnit.Packs, 2);
            cart.AddProduct(_productFacory.Pasta(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.PastaSauce(), QuantityUnit.Packs, 1);
            cart.AddProduct(_productFacory.Cola(new AmountForThePriceOfProductAmount(3, 2)), QuantityUnit.Packs, 4);
            cart.AddProduct(_productFacory.MandMs(), QuantityUnit.Packs,
                7);

            decimal totalPrice = cart.CalculateTotalPrice();
            AssertDiff(totalPrice, new decimal(26.60));
        }
    }
}