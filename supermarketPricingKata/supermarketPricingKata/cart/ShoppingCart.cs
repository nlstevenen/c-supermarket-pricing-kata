﻿using supermarketPricingKata.converter;
using supermarketPricingKata.discount;
using supermarketPricingKata.discount.cart;
using supermarketPricingKata.price;
using supermarketPricingKata.product;

namespace supermarketPricingKata.cart
{
    public class ShoppingCart
    {
        private readonly CartPriceCalculator _cartPriceCalculator;
        private readonly ProductPriceCalculator _productPriceCalculator;
        private readonly ShoppingCartProductHolder _shoppingCartProductHolder;
        private readonly UnitConverter _unitConverter;
        private ICartDiscount _cartDiscount;

        public ShoppingCart(ProductPriceCalculator productPriceCalculator, CartPriceCalculator cartPriceCalculator)
        {
            _productPriceCalculator = productPriceCalculator;
            _cartPriceCalculator = cartPriceCalculator;
            _unitConverter = new UnitConverter();
            _cartDiscount = new DefaultCartDiscount();
            _shoppingCartProductHolder = new ShoppingCartProductHolder();
        }

        public void AddProduct(WeightProduct product, WeightUnits unit, decimal quantity)
        {
            decimal productQuantity = ConvertProductUnitQuantity(product, unit, quantity);
            AddProductToCart(product, productQuantity);
        }

        public void AddProduct(QuantityProduct product, QuantityUnit unit, decimal quantity)
        {
            decimal productQuantity = ConvertProductUnitQuantity(product, unit, quantity);
            AddProductToCart(product, productQuantity);
        }

        private decimal ConvertProductUnitQuantity(WeightProduct product, WeightUnits unit, decimal quantity)
        {
            decimal productQuantity = quantity;
            if (product.GetUnit() != unit)
            {
                productQuantity = _unitConverter.convert(product, unit, quantity);
            }
            return productQuantity;
        }

        private decimal ConvertProductUnitQuantity(QuantityProduct product, QuantityUnit unit, decimal quantity)
        {
            decimal productQuantity = quantity;
            if (product.GetUnit() != unit)
            {
                productQuantity = _unitConverter.convert(product, unit, quantity);
            }
            return productQuantity;
        }

        private void AddProductToCart(IProduct product, decimal productQuantity)
        {
            _shoppingCartProductHolder.AddProduct(product, productQuantity);
        }

        public decimal CalculateTotalPrice()
        {
            decimal totalPrice = CalculateTotalPriceOfProducts();
            return CalculateTotalPriceOfCart(totalPrice);
        }

        private decimal CalculateTotalPriceOfCart(decimal totalPrice)
        {
            return _cartPriceCalculator.CalculatePrice(totalPrice, _cartDiscount);
        }

        private decimal CalculateTotalPriceOfProducts()
        {
            return _shoppingCartProductHolder.CalculateTotalPrice(_productPriceCalculator);
        }

        public void SetCartDiscount(ICartDiscount cartDiscount)
        {
            _cartDiscount = cartDiscount;
        }
    }
}