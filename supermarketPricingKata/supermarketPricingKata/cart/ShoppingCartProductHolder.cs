﻿using System.Collections.Generic;
using System.Linq;
using supermarketPricingKata.price;
using supermarketPricingKata.product;

namespace supermarketPricingKata.cart
{
    public class ShoppingCartProductHolder
    {
        private readonly Dictionary<IProduct, decimal> _products;

        public ShoppingCartProductHolder()
        {
            _products = new Dictionary<IProduct, decimal>();
        }

        public void AddProduct(IProduct product, decimal productQuantity)
        {
            if (_products.ContainsKey(product))
            {
                _products[product] += productQuantity;
            }
            else
            {
                _products.Add(product, productQuantity);
            }
        }


        public decimal CalculateTotalPrice(IProductPriceCalculator iproductPriceCalculator)
        {
            decimal totalPrice =
                _products.Sum(
                    keyValuePair => iproductPriceCalculator.CalculatePrice(keyValuePair.Key, keyValuePair.Value));
            return totalPrice;
        }
    }
}