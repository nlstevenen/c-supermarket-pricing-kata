﻿namespace supermarketPricingKata.converter
{
    public enum QuantityUnit
    {
        Packs = 1,
        Bottle = 1,
        Sack = 1,
        Jar = 1
    }
}