﻿using supermarketPricingKata.product;

namespace supermarketPricingKata.converter
{
    public class UnitConverter
    {
        public decimal convert(WeightProduct product, WeightUnits unit, decimal quantity)
        {
            var productUnit = (decimal) product.GetUnit();
            var fromUnit = (decimal) unit;
            return ConvertUnit(quantity, productUnit, fromUnit);
        }

        private static decimal ConvertUnit(decimal quantity, decimal productUnit, decimal fromUnit)
        {
            return (productUnit/fromUnit)*quantity;
        }

        public decimal convert(QuantityProduct product, QuantityUnit unit, decimal quantity)
        {
            var productUnit = (decimal) product.GetUnit();
            var fromUnit = (decimal) unit;
            return ConvertUnit(quantity, productUnit, fromUnit);
        }
    }
}