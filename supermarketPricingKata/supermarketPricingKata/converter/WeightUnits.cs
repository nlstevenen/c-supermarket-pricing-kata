﻿namespace supermarketPricingKata.converter
{
    public enum WeightUnits
    {
        Kilo = 1,
        Gram = 1000,
        Ounce = 10, // Dutch ounce: 1kilo = 2 Pond = 10 Ounce
        Pound = 2
    }
}