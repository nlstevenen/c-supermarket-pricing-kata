﻿using supermarketPricingKata.product;

namespace supermarketPricingKata.price
{
    public class ProductPriceCalculator : IProductPriceCalculator
    {
        public decimal CalculatePrice(IProduct iProduct, decimal quantity)
        {
            if (iProduct.GetDiscount() != null)
            {
                return iProduct.GetDiscount().CalculateTotalPrice(iProduct, quantity);
            }
            return iProduct.GetUnitPrice()*quantity;
        }
    }
}