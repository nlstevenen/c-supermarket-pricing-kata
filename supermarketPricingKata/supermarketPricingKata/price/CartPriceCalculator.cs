﻿using supermarketPricingKata.discount;

namespace supermarketPricingKata.price
{
    public class CartPriceCalculator
    {
        public decimal CalculatePrice(decimal totalPrice, ICartDiscount cartDiscount)
        {
            return cartDiscount.calculateTotalPrice(totalPrice);
        }
    }
}