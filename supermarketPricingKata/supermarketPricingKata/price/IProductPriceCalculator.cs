﻿using supermarketPricingKata.product;

namespace supermarketPricingKata.price
{
    public interface IProductPriceCalculator
    {
        decimal CalculatePrice(IProduct key, decimal quantity);
    }
}