﻿/**
 * Product Factory Façade
 */

using supermarketPricingKata.discount;

namespace supermarketPricingKata.product.facories
{
    public class ProductFacory
    {
        private readonly QuantityProductFactory _quantityProductFactory;
        private readonly WeightProductFactory _weightProductFactory;

        public ProductFacory()
        {
            _quantityProductFactory = new QuantityProductFactory();
            _weightProductFactory = new WeightProductFactory();
        }

        public WeightProduct Apple(IProductDiscount iProductDiscount = null)
        {
            return _weightProductFactory.Apple(iProductDiscount);
        }

        public QuantityProduct Cola(IProductDiscount iProductDiscount = null)
        {
            return _quantityProductFactory.Cola(iProductDiscount);
        }

        public QuantityProduct MandMs(IProductDiscount iProductDiscount = null)
        {
            return _quantityProductFactory.MandMs(iProductDiscount);
        }

        public QuantityProduct Milk(IProductDiscount iProductDiscount = null)
        {
            return _quantityProductFactory.Milk(iProductDiscount);
        }

        public QuantityProduct Pasta(IProductDiscount iProductDiscount = null)
        {
            return _quantityProductFactory.Pasta(iProductDiscount);
        }

        public QuantityProduct PastaSauce(IProductDiscount iProductDiscount = null)
        {
            return _quantityProductFactory.PastaSauce(iProductDiscount);
        }

        public WeightProduct Rice(IProductDiscount iProductDiscount = null)
        {
            return _weightProductFactory.Rice(iProductDiscount);
        }
    }
}