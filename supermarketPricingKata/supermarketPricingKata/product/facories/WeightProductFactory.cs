﻿using supermarketPricingKata.converter;
using supermarketPricingKata.discount;

namespace supermarketPricingKata.product.facories
{
    public class WeightProductFactory : AbstractProductFacory
    {
        public WeightProduct Apple(IProductDiscount iProductDiscount = null)
        {
            iProductDiscount = getProductDiscount(iProductDiscount);
            return new WeightProduct("Apple", WeightUnits.Kilo, new decimal(2.49), iProductDiscount);
        }

        public WeightProduct Rice(IProductDiscount iProductDiscount = null)
        {
            iProductDiscount = getProductDiscount(iProductDiscount);
            return new WeightProduct("Rice", WeightUnits.Gram, new decimal(0.2), iProductDiscount);
        }
    }
}