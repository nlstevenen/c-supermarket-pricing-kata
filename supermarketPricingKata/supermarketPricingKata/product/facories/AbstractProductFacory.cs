﻿using supermarketPricingKata.discount;
using supermarketPricingKata.discount.product;

namespace supermarketPricingKata.product.facories
{
    public class AbstractProductFacory
    {
        protected IProductDiscount getProductDiscount(IProductDiscount productDiscount)
        {
            if (productDiscount == null)
            {
                return new DefaultProductDiscount();
            }
            return productDiscount;
        }
    }
}