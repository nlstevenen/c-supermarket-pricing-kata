﻿using supermarketPricingKata.converter;
using supermarketPricingKata.discount;

namespace supermarketPricingKata.product.facories
{
    public class QuantityProductFactory : AbstractProductFacory
    {
        public QuantityProduct Cola(IProductDiscount productDiscount = null)
        {
            productDiscount = getProductDiscount(productDiscount);
            return new QuantityProduct("Cola", QuantityUnit.Bottle, new decimal(1.50), productDiscount);
        }

        public QuantityProduct MandMs(IProductDiscount productDiscount = null)
        {
            productDiscount = getProductDiscount(productDiscount);
            return new QuantityProduct("MandMs", QuantityUnit.Sack, new decimal(2.10), productDiscount);
        }

        public QuantityProduct Milk(IProductDiscount productDiscount = null)
        {
            productDiscount = getProductDiscount(productDiscount);
            return new QuantityProduct("Milk", QuantityUnit.Packs, new decimal(1.29), productDiscount);
        }

        public QuantityProduct Pasta(IProductDiscount productDiscount = null)
        {
            productDiscount = getProductDiscount(productDiscount);
            return new QuantityProduct("Pasta", QuantityUnit.Packs, new decimal(0.99), productDiscount);
        }

        public QuantityProduct PastaSauce(IProductDiscount productDiscount = null)
        {
            productDiscount = getProductDiscount(productDiscount);
            return new QuantityProduct("Pasta sauce", QuantityUnit.Jar, new decimal(1.39), productDiscount);
        }
    }
}