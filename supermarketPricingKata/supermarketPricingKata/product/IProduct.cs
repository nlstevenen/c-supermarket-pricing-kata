﻿using supermarketPricingKata.discount;

namespace supermarketPricingKata.product
{
    public interface IProduct
    {
        decimal GetUnitPrice();
        void SetDiscount(IProductDiscount discount);
        IProductDiscount GetDiscount();
    }
}