﻿using System;
using supermarketPricingKata.converter;
using supermarketPricingKata.discount;

namespace supermarketPricingKata.product
{
    public class QuantityProduct : IProduct
    {
        private readonly String _productName;
        private readonly QuantityUnit _productUnit;
        private readonly decimal _productUnitPrice;
        private IProductDiscount _productDiscount;

        public QuantityProduct(string productName, QuantityUnit productUnit, decimal productUnitPrice,
            IProductDiscount productDiscount)
        {
            _productName = productName;
            _productUnit = productUnit;
            _productUnitPrice = productUnitPrice;
            _productDiscount = productDiscount;
        }

        public decimal GetUnitPrice()
        {
            return _productUnitPrice;
        }

        public void SetDiscount(IProductDiscount discount)
        {
            _productDiscount = discount;
        }

        public IProductDiscount GetDiscount()
        {
            return _productDiscount;
        }

        public QuantityUnit GetUnit()
        {
            return _productUnit;
        }
    }
}