﻿using supermarketPricingKata.converter;
using supermarketPricingKata.discount;
using supermarketPricingKata.discount.product;

namespace supermarketPricingKata.product
{
    public abstract class Product : IProduct
    {
        protected IProductDiscount ProductDiscount;

        protected Product()
        {
            ProductDiscount = new DefaultProductDiscount();
        }

        public Product(IProductDiscount productDiscount)
        {
            ProductDiscount = productDiscount;
        }

        public abstract decimal GetUnitPrice();
        public abstract void SetDiscount(IProductDiscount discount);
        public abstract IProductDiscount GetDiscount();
        public abstract QuantityUnit GetUnit();

        protected bool Equals(Product other)
        {
            if (GetType() == other.GetType())
                return true;
            return false;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Product) obj);
        }

        public override int GetHashCode()
        {
            return 304573945;
        }
    }
}