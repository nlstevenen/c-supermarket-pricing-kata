﻿using System;
using supermarketPricingKata.converter;
using supermarketPricingKata.discount;

namespace supermarketPricingKata.product
{
    public class WeightProduct : IProduct
    {
        private readonly String _productName;
        private readonly WeightUnits _productUnit;
        private readonly decimal _productUnitPrice;
        private IProductDiscount _productDiscount;

        public WeightProduct(string productName, WeightUnits productUnit, decimal productUnitPrice,
            IProductDiscount productDisctount)
        {
            _productName = productName;
            _productUnit = productUnit;
            _productUnitPrice = productUnitPrice;
            _productDiscount = productDisctount;
        }

        public decimal GetUnitPrice()
        {
            return _productUnitPrice;
        }

        public void SetDiscount(IProductDiscount discount)
        {
            _productDiscount = discount;
        }

        public IProductDiscount GetDiscount()
        {
            return _productDiscount;
        }

        public WeightUnits GetUnit()
        {
            return _productUnit;
        }
    }
}