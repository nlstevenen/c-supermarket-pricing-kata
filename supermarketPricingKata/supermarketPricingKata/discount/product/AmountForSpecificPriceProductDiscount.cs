﻿using System;
using supermarketPricingKata.product;

namespace supermarketPricingKata.discount.product
{
    public class AmountForSpecificPriceProductDiscount : IProductDiscount
    {
        private readonly decimal _amount;
        private readonly decimal _price;

        public AmountForSpecificPriceProductDiscount(decimal amount, decimal price)
        {
            _amount = amount;
            _price = price;
        }

        public decimal CalculateTotalPrice(IProduct product, decimal quantity)
        {
            decimal numberOfDiscounts = Math.Floor(quantity/_amount);
            decimal totalPrice = numberOfDiscounts*_price;

            decimal restAmount = quantity%_amount;
            restAmount *= product.GetUnitPrice();

            return totalPrice + restAmount;
        }
    }
}