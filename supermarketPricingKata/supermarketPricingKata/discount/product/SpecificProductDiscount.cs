﻿using supermarketPricingKata.product;

namespace supermarketPricingKata.discount.product
{
    public class SpecificProductDiscount : IProductDiscount
    {
        private readonly decimal _discountPercentage;

        public SpecificProductDiscount(decimal dicountPercentage)
        {
            _discountPercentage = dicountPercentage;
        }

        public decimal CalculateTotalPrice(IProduct product, decimal quantity)
        {
            decimal unitPrice = product.GetUnitPrice();
            decimal discount = unitPrice*(_discountPercentage/100);
            return (unitPrice -= discount)*quantity;
        }
    }
}