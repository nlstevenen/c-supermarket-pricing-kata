﻿using System;
using supermarketPricingKata.product;

namespace supermarketPricingKata.discount.product
{
    /**
     * Example: Three for the Price Of Two
     */

    public class AmountForThePriceOfProductAmount : IProductDiscount
    {
        private readonly decimal _amount;
        private readonly decimal _productAmount;

        public AmountForThePriceOfProductAmount(decimal amount, decimal productAmount)
        {
            _amount = amount;
            _productAmount = productAmount;
        }

        public decimal CalculateTotalPrice(IProduct product, decimal quantity)
        {
            decimal numberOfDiscounts = Math.Floor(quantity/_amount);
            decimal totalDiscountPrice = product.GetUnitPrice()*_productAmount*numberOfDiscounts;
            decimal resultingProductsPrice = (quantity%_amount)*product.GetUnitPrice();
            return totalDiscountPrice + resultingProductsPrice;
        }
    }
}