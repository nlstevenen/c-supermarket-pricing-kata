﻿using supermarketPricingKata.product;

namespace supermarketPricingKata.discount
{
    public interface IProductDiscount : IDiscount
    {
        decimal CalculateTotalPrice(IProduct product, decimal quantity);
    }
}