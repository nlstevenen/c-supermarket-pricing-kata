﻿namespace supermarketPricingKata.discount.cart
{
    internal class DefaultCartDiscount : ICartDiscount
    {
        public decimal calculateTotalPrice(decimal totalPrice)
        {
            return totalPrice;
        }
    }
}