﻿namespace supermarketPricingKata.discount
{
    public interface ICartDiscount : IDiscount
    {
        decimal calculateTotalPrice(decimal totalPrice);
    }
}