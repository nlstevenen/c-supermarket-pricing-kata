﻿namespace supermarketPricingKata.discount.cart
{
    public class SpecificCartDiscount : ICartDiscount
    {
        private readonly decimal _discountPercentage;

        public SpecificCartDiscount(decimal discountPercentage)
        {
            _discountPercentage = discountPercentage;
        }

        public decimal calculateTotalPrice(decimal totalPrice)
        {
            decimal discount = totalPrice*(_discountPercentage/100);
            return totalPrice - discount;
        }
    }
}