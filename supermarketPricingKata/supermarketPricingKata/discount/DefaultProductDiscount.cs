﻿using supermarketPricingKata.product;

namespace supermarketPricingKata.discount.product
{
    internal class DefaultProductDiscount : IProductDiscount
    {
        public decimal CalculateTotalPrice(IProduct product, decimal quantity)
        {
            return product.GetUnitPrice()*quantity;
        }
    }
}